Fletcher
========

What is this?
-------------

A library translating between fixed length text and Java Objects.  
Fletcher is abbreviations of 'Fixed LEngth Text CHaractEs Reader'.

System Requirements
-------------------

1. Java SE 7 or later
2. Maven 3.x

