/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.dto;

import org.bitbucket.f97one.fletcher.annotations.TextRecordField;

/**
 * A DTO to retain the data length and encoding of each field defined in the annotation.
 * 
 * @author HAJIME
 */
public class FieldInfo {
    private int startPosition;
    private int length;
    private TextRecordField.Alignment alignment;
    private boolean zeroPadding;

    public boolean isZeroPadding() {
        return zeroPadding;
    }

    public void setZeroPadding(boolean zeroPadding) {
        this.zeroPadding = zeroPadding;
    }

    public FieldInfo() {
        this.startPosition = 0;
        this.length = 1;
        this.alignment = TextRecordField.Alignment.Left;
    }

    public FieldInfo(int startPosition, int length) {
        this.startPosition = startPosition;
        this.length = length;
        this.alignment = TextRecordField.Alignment.Left;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public TextRecordField.Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(TextRecordField.Alignment alignment) {
        this.alignment = alignment;
    }
}
