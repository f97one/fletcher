/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.dto;

/**
 * A DTO to retain the whole data length and encoding defined in the annotation.
 * 
 * @author HAJIME Fukuna
 */
public class TypeInfo {
    private int length;
    private String encoding;

    public TypeInfo() {
        this.length = 0;
        this.encoding = "utf-8";
    }

    public TypeInfo(int length, String encoding) {
        this.length = length;
        this.encoding = encoding;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
    
}
