/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bitbucket.f97one.fletcher.annotations.TextRecordField;
import org.bitbucket.f97one.fletcher.dto.FieldInfo;
import org.bitbucket.f97one.fletcher.dto.TypeInfo;

/**
 * A class to transform into any class from fixed length text string
 * (or vice versa).<br>
 * Fletcher is abbreviations of 'Fixed LEngth Text CHaractEs Reader'.
 * 
 * @author HAJIME Fukuna
 * @since 2016/02/11
 */
public class Fletcher {

    /**
     * Encodings
     */
    public static class Encodings {
        private Encodings() {
            
        }
        
        /**
         * Encoding : UTF-8
         */
        public static final String UTF8 = "utf-8";
        /**
         * Encoding : MS932(Alias of Shift-JIS)
         */
        public static final String MS932 = "ms932";
        /**
         * Encoding : Shift-JIS
         */
        public static final String SHIFT_JIS = "shift_jis";
        /**
         * Encoding : Windows31-J(Alias of Shift-JIS)
         */
        public static final String WIN31_J = "windows31-j";
    }
    
    /**
     * Constructor.<br>
     * Creates a new instance normally.
     */
    public Fletcher() {
        
    }
    
    /**
     * Transforms into any class from fixed length text string.
     * 
     * @param <T> A type parameter to wish to transform into.
     * @param line String to wish to transform into.
     * @param type A type to wish to transform into.
     * @return An object translated from fixed length text.
     * @throws IllegalArgumentException Thrown when number of characters and the
     * data length of the line does not match.
     */
    public <T> T fromStr(String line, Class<T> type) {
        T entity = null;
        
        try {
            TypeInfo typeInfo = AnnotationReader.readTypeInfo(type);
            byte[] stream = line.getBytes(typeInfo.getEncoding());
            
            if (line.length() % typeInfo.getLength() != 0) {
                if (!typeInfo.getEncoding().equals(Encodings.UTF8)) {

                    if (stream.length % typeInfo.getLength() != 0) {
                        throw new IllegalArgumentException("Number of characters and the data length of the line does not match.");
                    }

                }
            }
            
            entity = type.newInstance();
            
            Field[] fields = entity.getClass().getDeclaredFields();
            
            for (Field f : fields) {
                f.setAccessible(true);
                FieldInfo fieldInfo = AnnotationReader.readFieldInfo(f);
                
                int sp = fieldInfo.getStartPosition();
                int ep = fieldInfo.getStartPosition() + fieldInfo.getLength();
                
                String param;
                
                if (typeInfo.getEncoding().equals(Encodings.UTF8)) {
                    param = line.substring(sp, ep).trim();
                } else {
                    param = new String(Arrays.copyOfRange(stream, sp, ep), typeInfo.getEncoding()).trim();
                }

                Class<?> t = f.getType();
                String tn = getTypeName(t).toLowerCase();

                if (tn.endsWith("int") || tn.endsWith("integer")) {
                    f.set(entity, Integer.parseInt(param));
                } else if (tn.endsWith("long")) {
                    f.set(entity, Long.parseLong(param));
                } else if (tn.endsWith("short")) {
                    f.set(entity, Short.parseShort(param));
                } else if (tn.endsWith("string")) {
                    f.set(entity, param);
                } else if (tn.endsWith("float")) {
                    f.set(entity, Float.parseFloat(param));
                } else if (tn.endsWith("double")) {
                    f.set(entity, Double.parseDouble(param));
                } else if (tn.endsWith("char") || tn.endsWith("character")) {
                    f.set(entity, param.charAt(0));
                } else if (tn.endsWith("boolean")) {
                    f.set(entity, Boolean.parseBoolean(param));
                } else {
                    Logger.getLogger(Fletcher.class.getName()).log(Level.WARNING, "Could not find the field that can be applied. Found : " + tn);
                }

            }
        } catch (IllegalArgumentException | ReflectiveOperationException ex) {
            Logger.getLogger(Fletcher.class.getName()).log(Level.SEVERE, null, ex);
            
            entity = null;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Fletcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return entity;
    }

    /**
     * Transforms into any class from fixed length text string.
     *
     * @param <T> A type parameter to wish to transform into.
     * @param encodedStream byte arrays to wish to transform into.
     * @param type A type to wish to transform into.
     * @return An object translated from fixed length text.
     * @throws IllegalArgumentException Thrown when number of characters and the
     * data length of the line does not match.
     */
    public <T> T fromBytes(byte[] encodedStream, Class<T> type) {
        T entity = null;
        
        try {
            TypeInfo typeInfo = AnnotationReader.readTypeInfo(type);
            
            if (encodedStream.length % typeInfo.getLength() != 0) {
                throw new IllegalArgumentException("Number of characters and the data length of the line does not match.");
            }
            
            entity = type.newInstance();
            
            Field[] fields = entity.getClass().getDeclaredFields();
            
            for (Field f : fields) {
                f.setAccessible(true);
                
                FieldInfo fieldInfo = AnnotationReader.readFieldInfo(f);

                int sp = fieldInfo.getStartPosition();
                int ep = fieldInfo.getStartPosition() + fieldInfo.getLength();

                byte[] ranged = Arrays.copyOfRange(encodedStream, sp, ep);
                String param = new String(ranged).trim();
                
                Class<?> t = f.getType();
                String tn = getTypeName(t).toLowerCase();

                if (tn.endsWith("int") || tn.endsWith("integer")) {
                    f.set(entity, Integer.parseInt(param));
                } else if (tn.endsWith("long")) {
                    f.set(entity, Long.parseLong(param));
                } else if (tn.endsWith("short")) {
                    f.set(entity, Short.parseShort(param));
                } else if (tn.endsWith("string")) {
                    f.set(entity, param);
                } else if (tn.endsWith("float")) {
                    f.set(entity, Float.parseFloat(param));
                } else if (tn.endsWith("double")) {
                    f.set(entity, Double.parseDouble(param));
                } else if (tn.endsWith("char") || tn.endsWith("character")) {
                    f.set(entity, param.charAt(0));
                } else if (tn.endsWith("boolean")) {
                    f.set(entity, Boolean.parseBoolean(param));
                } else {
                    Logger.getLogger(Fletcher.class.getName()).log(Level.WARNING, "Could not find the field that can be applied. Found : " + tn);
                }
                
            }
            
        } catch (IllegalArgumentException | InstantiationException | IllegalAccessException | SecurityException ex) {
            Logger.getLogger(Fletcher.class.getName()).log(Level.SEVERE, null, ex);

            entity = null;
        }
        
        return entity;
    }
    
    private String getTypeName(Class type) {
        if (!type.isArray()) {
            return type.getName();
        }
        return getTypeName(type.getComponentType()) + "[]";
    }

    /**
     * Transforms into fixed-length text from any entity.
     * 
     * @param <T> A type parameter to wish to transform into.
     * @param entity A entity to wish to transform into fixed-length text.
     * @return Fixed-length text delived from entity.
     */
    public <T> String transform(T entity) {
        
        TypeInfo typeInfo = AnnotationReader.readTypeInfo(entity.getClass());
        
        if (!typeInfo.getEncoding().equals(Encodings.UTF8)) {
            try {
                return new String(transformToBin(entity), typeInfo.getEncoding());
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(Fletcher.class.getName()).log(Level.SEVERE, null, ex);
                throw new RuntimeException(ex);
            }
        }
        
        StringBuilder builder = new StringBuilder(appendWhiteSpace("", TextRecordField.Alignment.Left, typeInfo.getLength()));
        
        Field[] fields = entity.getClass().getDeclaredFields();
        
        for (Field f : fields) {
            f.setAccessible(true);
            FieldInfo fieldInfo = AnnotationReader.readFieldInfo(f);
            
            if (fieldInfo == null) {
                continue;
            }

            int sp = fieldInfo.getStartPosition();
            int ep = fieldInfo.getStartPosition() + fieldInfo.getLength();

            Class<?> t = f.getType();
            String tn = getTypeName(t).toLowerCase();
            
            String appender;
            
            try {
                if (tn.endsWith("int") || tn.endsWith("integer")) {
                    Integer i = (Integer) f.get(entity);
                    appender = mkAppender(i, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);

                } else if (tn.endsWith("long")) {
                    Long l = (Long) f.get(entity);
                    appender = mkAppender(l, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);
                    
                } else if (tn.endsWith("short")) {
                    Short s = (Short) f.get(entity);
                    appender = mkAppender(s, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);

                } else if (tn.endsWith("string")) {
                    String s = (String) f.get(entity);
                    appender = mkAppender(s, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);

                } else if (tn.endsWith("float")) {
                    Float fl = (Float) f.get(entity);
                    appender = mkAppender(fl, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);

                } else if (tn.endsWith("double")) {
                    Double d = (Double) f.get(entity);
                    appender = mkAppender(d, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);

                } else if (tn.endsWith("char") || tn.endsWith("character")) {
                    Character c = (Character) f.get(entity);
                    appender = mkAppender(c, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);

                } else if (tn.endsWith("boolean")) {
                    Boolean b = (Boolean) f.get(entity);
                    appender = mkAppender(b, fieldInfo, typeInfo);
                    builder.replace(sp, ep, appender);

                } else {
                    Logger.getLogger(Fletcher.class.getName()).log(Level.WARNING, "Could not find the field that can be applied. Found : " + tn);
                }
            } catch (IllegalArgumentException | ReflectiveOperationException ex) {
                Logger.getLogger(Fletcher.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
        return builder.toString();
    }
    
    /**
     * Transforms into fixed-length text from any entity.
     *
     * @param <T> A type parameter to wish to transform into.
     * @param entity A entity to wish to transform into fixed-length text.
     * @return byte arrays delived from entity.
     */
    public <T> byte[] transformToBin(T entity) {
        TypeInfo typeInfo = AnnotationReader.readTypeInfo(entity.getClass());

        byte[] baseBytes = new byte[typeInfo.getLength()];
        
        try {
            Field[] fields = entity.getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                FieldInfo fieldInfo = AnnotationReader.readFieldInfo(f);
                
                if (fieldInfo == null) {
                    continue;
                }
                
                int sp = fieldInfo.getStartPosition();
                int ep = fieldInfo.getStartPosition() + fieldInfo.getLength();
                
                Class<?> t = f.getType();
                String tn = getTypeName(t).toLowerCase();
                
                byte[] appender;
                
                if (tn.endsWith("int") || tn.endsWith("integer")) {
                    Integer i = (Integer) f.get(entity);
                    appender = mkAppender(i, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else if (tn.endsWith("long")) {
                    Long l = (Long) f.get(entity);
                    appender = mkAppender(l, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else if (tn.endsWith("short")) {
                    Short s = (Short) f.get(entity);
                    appender = mkAppender(s, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else if (tn.endsWith("string")) {
                    String s = (String) f.get(entity);
                    appender = mkAppender(s, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else if (tn.endsWith("float")) {
                    Float fl = (Float) f.get(entity);
                    appender = mkAppender(fl, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else if (tn.endsWith("double")) {
                    Double d = (Double) f.get(entity);
                    appender = mkAppender(d, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else if (tn.endsWith("char") || tn.endsWith("character")) {
                    Character c = (Character) f.get(entity);
                    appender = mkAppender(c, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else if (tn.endsWith("boolean")) {
                    Boolean b = (Boolean) f.get(entity);
                    appender = mkAppender(b, fieldInfo, typeInfo).getBytes(typeInfo.getEncoding());
                    
                    System.arraycopy(appender, 0, baseBytes, fieldInfo.getStartPosition(), appender.length);

                } else {
                    Logger.getLogger(Fletcher.class.getName()).log(Level.WARNING, "Could not find the field that can be applied. Found : " + tn);
                }
            }
        } catch (UnsupportedEncodingException | SecurityException | IllegalArgumentException | ReflectiveOperationException ex) {
            Logger.getLogger(Fletcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return baseBytes;
    }
    
    private String appendWhiteSpace(String base, TextRecordField.Alignment alignment, int length) {
        if (base.length() == length) {
            return base;
        } else if (base.length() > length) {
            throw new IllegalArgumentException("length must be larger than base.lenght().");
        }
        
        StringBuilder builder = new StringBuilder(base);
        int start;
        int end;
        if (alignment == TextRecordField.Alignment.Left) {
            start = base.length();
            end = length;
        } else {
            start = 0;
            end = length - base.length();
        }
        
        for (int i = start; i < end; i++) {
            builder.insert(i, " ");
        }
        
        return builder.toString();
    }
    
    private String getAppender(String s, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String ret = "";
        if (typeInfo.getEncoding().equals(Encodings.UTF8)) {
            ret = appendWhiteSpace(s, fieldInfo.getAlignment(), fieldInfo.getLength());
        } else {
            try {
                byte[] base = s.getBytes(typeInfo.getEncoding());
                
                if (base.length > fieldInfo.getLength()) {
                    String reason = String.format("Length of field value exceeded expected length, "
                            + "Anntated length = %d, but actual length = %d .", 
                            fieldInfo.getLength(), base.length);
                    throw new IllegalArgumentException(reason);
                }
                String padSp = appendWhiteSpace("", TextRecordField.Alignment.Left, fieldInfo.getLength() - base.length);
                
                byte[] pad = padSp.getBytes(typeInfo.getEncoding());
                
                byte[] concat = new byte[base.length + padSp.length()];
                
                if (fieldInfo.getAlignment() == TextRecordField.Alignment.Left) {
                    // 左詰めの場合は、値 -> スペースの順で結合
                    System.arraycopy(base, 0, concat, 0, base.length);
                    System.arraycopy(pad, 0, concat, base.length, pad.length);
                } else {
                    // 右詰めの場合は、スペース -> 値の順で結合
                    System.arraycopy(pad, 0, concat, 0, pad.length);
                    System.arraycopy(base, 0, concat, pad.length, base.length);
                }
                
                ret =  new String(concat, typeInfo.getEncoding());
                
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(Fletcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return ret;
    }
    
    private String mkAppender(Integer value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String base = String.valueOf(value);
        
        if (fieldInfo.isZeroPadding()) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < fieldInfo.getLength() - base.length(); i++) {
                builder.append(0);
            }
            builder.append(base);
            
            return getAppender(builder.toString(), fieldInfo, typeInfo);
        } else {
            return getAppender(base, fieldInfo, typeInfo);
        }
    }
    
    private String mkAppender(Long value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String base = String.valueOf(value);

        if (fieldInfo.isZeroPadding()) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < fieldInfo.getLength() - base.length(); i++) {
                builder.append(0);
            }
            builder.append(base);
            
            return getAppender(builder.toString(), fieldInfo, typeInfo);
        } else {
            return getAppender(base, fieldInfo, typeInfo);
        }
    }
    
    private String mkAppender(Short value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String base = String.valueOf(value);

        return getAppender(base, fieldInfo, typeInfo);
    }
    
    private String mkAppender(Float value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String base = String.valueOf(value);

        return getAppender(base, fieldInfo, typeInfo);
    }
    
    private String mkAppender(Double value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String base = String.valueOf(value);

        return getAppender(base, fieldInfo, typeInfo);
    }
    
    private String mkAppender(Boolean value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String base = String.valueOf(value);

        return getAppender(base, fieldInfo, typeInfo);
    }
    
    private String mkAppender(Character value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        String base = String.valueOf(value);

        return getAppender(base, fieldInfo, typeInfo);
    }
    
    private String mkAppender(String value, FieldInfo fieldInfo, TypeInfo typeInfo) {
        return getAppender(value, fieldInfo, typeInfo);
    }
}
