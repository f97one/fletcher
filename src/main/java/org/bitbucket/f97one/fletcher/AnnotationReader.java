/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import org.bitbucket.f97one.fletcher.annotations.FixedTextRecord;
import org.bitbucket.f97one.fletcher.annotations.TextRecordField;
import org.bitbucket.f97one.fletcher.dto.FieldInfo;
import org.bitbucket.f97one.fletcher.dto.TypeInfo;

/**
 *
 * @author HAJIME
 */
class AnnotationReader {
    
    private static TypeInfo sTypeInfo;
    private static String sTypeName;
    
    /**
     * Read annotated type information from DTO.
     * 
     * @param type Type to obtain information that is defined in the annotation.
     * @return DTO summarizing the information defined in the annotations.
     */
    static <T> TypeInfo readTypeInfo(Class<T> type) {
        if (sTypeInfo == null || !type.getName().equals(sTypeName)) {
            sTypeName = type.getName();
            
            sTypeInfo = new TypeInfo();
            
            Annotation[] clzAnnotations = type.getAnnotations();

            // フィールドのデータ長の合計を取得
            int lengthSum = 0;
            Field[] fields = type.getDeclaredFields();
            for (Field f : fields) {
                FieldInfo fi = readFieldInfo(f);
                
                if (fi != null) {
                    lengthSum += fi.getLength();
                }
            }
            
            for (Annotation a : clzAnnotations) {
                if (a instanceof FixedTextRecord) {
                    FixedTextRecord r = (FixedTextRecord) a;
                    
                    sTypeInfo.setLength(r.length());
                    sTypeInfo.setEncoding(r.encoding());
                    
                    break;
                }
            }

            // 型のデータ長とフィールドのデータ長の合計が一致しない場合、IlleagalArgumentExceptionを投げる
            if (lengthSum != sTypeInfo.getLength()) {
                throw new IllegalArgumentException("Sum of TextRecordField#length() and FixedTextRecord#length() do not match.");
            }
        }
        
        return sTypeInfo;
    }
    
    /**
     * Read annotated type information from decrared field.
     * 
     * @param field Field to obtain information that is defined in the annotation.
     * @return DTO summarizing the information defined in the annotations, or null if field doesn't have annotation.
     */
    static FieldInfo readFieldInfo(Field field) {
        FieldInfo fi = null;

        Annotation[] fieldAnnotations = field.getDeclaredAnnotations();

        for (Annotation a : fieldAnnotations) {
            if (a instanceof TextRecordField) {
                fi = new FieldInfo();
                TextRecordField f = (TextRecordField) a;

                fi.setStartPosition(f.startPosition());
                fi.setLength(f.length());
                fi.setAlignment(f.alignmentDirection());
                fi.setZeroPadding(f.zeroPadding());

                break;
            }
        }
        
        return fi;
    }
}
