/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author HAJIME Fukuna
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TextRecordField {

    /**
     * Defines the direction of the justifying string. 
     */
    public enum Alignment {
        /**
         * Sets a string to left-justified.
         */
        Left,
        /**
         * Sets a string to right-justified.
         */
        Right
    }

    /**
     * Sets start position of a field.
     * 
     * @return start position of field, if omitted, default value is 0
     */
    int startPosition() default 0;

    /**
     * Sets length of a field.
     * 
     * @return length of a field, if omitted, default value is 1.
     */
    int length() default 1;

    /**
     * Sets direction of alignment of string.
     * 
     * @return direction of alignment of string, if omitted, default value is Left.
     */
    Alignment alignmentDirection() default Alignment.Left;
    
    /**
     * To set true when pack zero to the beginning.
     * 
     * @return if true, pack zero to the beginning, if omitted, default value is false.
     */
    boolean zeroPadding() default false;
}
