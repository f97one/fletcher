/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation to define whole text record condition.
 * 
 * @author HAJIME Fukuna
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FixedTextRecord {
    /**
     * Length of per record.
     * 
     * @return length of per record, if omitted, default is 1.
     */
    int length() default 1;

    /**
     * Encoding regarded as default.
     * 
     * @return encoding regarded as default, if omitted, default is utf-8.
     */
    String encoding() default "utf-8";
}
