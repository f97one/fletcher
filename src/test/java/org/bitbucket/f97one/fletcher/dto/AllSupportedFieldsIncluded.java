/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.dto;

import org.bitbucket.f97one.fletcher.annotations.FixedTextRecord;
import org.bitbucket.f97one.fletcher.annotations.TextRecordField;

/**
 *
 * @author f97one
 */
@FixedTextRecord(length = 82)
public class AllSupportedFieldsIncluded {
    
    @TextRecordField(startPosition = 0, length = 4)
    private int intField01;
    
    @TextRecordField(startPosition = 4, length = 4)
    private Integer integerField01;
    
    @TextRecordField(startPosition = 8, length = 8)
    private long longField01;

    @TextRecordField(startPosition = 16, length = 8)
    private Long longField02;

    @TextRecordField(startPosition = 24, length = 2)
    private short shortField01;

    @TextRecordField(startPosition = 26, length = 2)
    private Short shortField02;

    @TextRecordField(startPosition = 28, length = 8)
    private String stringField01;

    @TextRecordField(startPosition = 36, length = 8)
    private float floatField01;

    @TextRecordField(startPosition = 44, length = 8)
    private Float floatField02;

    @TextRecordField(startPosition = 52, length = 8)
    private double dblField01;

    @TextRecordField(startPosition = 60, length = 8)
    private Double dblField02;

    @TextRecordField(startPosition = 68, length = 1)
    private char charField01;

    @TextRecordField(startPosition = 69, length = 1)
    private Character charField02;

    @TextRecordField(startPosition = 70, length = 6)
    private boolean boolField01;

    @TextRecordField(startPosition = 76, length = 6)
    private Boolean boolField02;

    public int getIntField01() {
        return intField01;
    }

    public void setIntField01(int intField01) {
        this.intField01 = intField01;
    }

    public Integer getIntegerField01() {
        return integerField01;
    }

    public void setIntegerField01(Integer integerField01) {
        this.integerField01 = integerField01;
    }

    public long getLongField01() {
        return longField01;
    }

    public void setLongField01(long longField01) {
        this.longField01 = longField01;
    }

    public Long getLongField02() {
        return longField02;
    }

    public void setLongField02(Long longField02) {
        this.longField02 = longField02;
    }

    public short getShortField01() {
        return shortField01;
    }

    public void setShortField01(short shortField01) {
        this.shortField01 = shortField01;
    }

    public Short getShortField02() {
        return shortField02;
    }

    public void setShortField02(Short shortField02) {
        this.shortField02 = shortField02;
    }

    public String getStringField01() {
        return stringField01;
    }

    public void setStringField01(String stringField01) {
        this.stringField01 = stringField01;
    }

    public float getFloatField01() {
        return floatField01;
    }

    public void setFloatField01(float floatField01) {
        this.floatField01 = floatField01;
    }

    public Float getFloatField02() {
        return floatField02;
    }

    public void setFloatField02(Float floatField02) {
        this.floatField02 = floatField02;
    }

    public double getDblField01() {
        return dblField01;
    }

    public void setDblField01(double dblField01) {
        this.dblField01 = dblField01;
    }

    public Double getDblField02() {
        return dblField02;
    }

    public void setDblField02(Double dblField02) {
        this.dblField02 = dblField02;
    }

    public char getCharField01() {
        return charField01;
    }

    public void setCharField01(char charField01) {
        this.charField01 = charField01;
    }

    public Character getCharField02() {
        return charField02;
    }

    public void setCharField02(Character charField02) {
        this.charField02 = charField02;
    }

    public boolean isBoolField01() {
        return boolField01;
    }

    public void setBoolField01(boolean boolField01) {
        this.boolField01 = boolField01;
    }

    public Boolean getBoolField02() {
        return boolField02;
    }

    public void setBoolField02(Boolean boolField02) {
        this.boolField02 = boolField02;
    }
    
    
}
