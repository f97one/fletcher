/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.dto;

import org.bitbucket.f97one.fletcher.Fletcher;
import org.bitbucket.f97one.fletcher.annotations.FixedTextRecord;
import org.bitbucket.f97one.fletcher.annotations.TextRecordField;

/**
 *
 * @author HAJIME
 */
@FixedTextRecord(length = 51, encoding = Fletcher.Encodings.MS932)
public class UserInfoMS932 {
    @TextRecordField(startPosition = 0, length = 8)
    private String userId;

    @TextRecordField(startPosition = 8, length = 8, alignmentDirection = TextRecordField.Alignment.Left)
    private String passwd;

    @TextRecordField(startPosition = 16, length = 32, alignmentDirection = TextRecordField.Alignment.Right)
    private String userName;

    @TextRecordField(startPosition = 48, length = 3)
    private int age;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
