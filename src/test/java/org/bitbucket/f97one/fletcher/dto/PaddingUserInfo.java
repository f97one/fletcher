/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.dto;

import org.bitbucket.f97one.fletcher.Fletcher;
import org.bitbucket.f97one.fletcher.annotations.FixedTextRecord;
import org.bitbucket.f97one.fletcher.annotations.TextRecordField;

/**
 *
 * @author HAJIME Fukuna
 */
@FixedTextRecord(length = 51, encoding = Fletcher.Encodings.UTF8)
public class PaddingUserInfo {

    @TextRecordField(startPosition = 0, length = 8, zeroPadding = true)
    private long userId;
    
    @TextRecordField(startPosition = 8, length = 8, alignmentDirection = TextRecordField.Alignment.Left)
    private String passwd;

    @TextRecordField(startPosition = 16, length = 32, alignmentDirection = TextRecordField.Alignment.Right)
    private String userName;

    @TextRecordField(startPosition = 48, length = 3, zeroPadding = true)
    private int age;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    
}
