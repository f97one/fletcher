/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher.dto;

import org.bitbucket.f97one.fletcher.Fletcher;
import org.bitbucket.f97one.fletcher.annotations.FixedTextRecord;
import org.bitbucket.f97one.fletcher.annotations.TextRecordField;

/**
 *
 * @author HAJIME
 */
@FixedTextRecord(length = 30, encoding = Fletcher.Encodings.UTF8)
public class IlleagalDto {
    
    @TextRecordField(startPosition = 0, length = 10)
    private String field1;
    @TextRecordField(startPosition = 10, length = 10)
    private String field2;
    
    private int nonAnnonField1;

    public int getNonAnnonField1() {
        return nonAnnonField1;
    }

    public void setNonAnnonField1(int nonAnnonField1) {
        this.nonAnnonField1 = nonAnnonField1;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }
    
}
