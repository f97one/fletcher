/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher;

import org.bitbucket.f97one.fletcher.AnnotationReader;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bitbucket.f97one.fletcher.annotations.TextRecordField;
import org.bitbucket.f97one.fletcher.dto.FieldInfo;
import org.bitbucket.f97one.fletcher.dto.IlleagalDto;
import org.bitbucket.f97one.fletcher.dto.TypeInfo;
import org.bitbucket.f97one.fletcher.dto.UserInfoMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HAJIME
 */
public class AnnotationReaderTest {
    
    public AnnotationReaderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void UserInfoMockの型のアノテーションが読み出せている() {
        System.out.println("readTypeInfo");
        TypeInfo result = AnnotationReader.readTypeInfo(UserInfoMock.class);

        assertEquals("データ長が51で読み出せている", 51, result.getLength());
        assertEquals("エンコーディングがutf-8で読み出せている", "utf-8", result.getEncoding());
    }
    
    @Test
    public void フィールド長とデータ長が一致しないDTOの場合例外を投げる() {
        try {
            TypeInfo result = AnnotationReader.readTypeInfo(IlleagalDto.class);
            
            fail("異常設定のDTOを読んでもそのまま通っている");
        } catch (Exception e) {
            assertTrue("IllegalArgumentExceptionを投げている", e instanceof IllegalArgumentException);
        }
    }

    @Test
    public void UserInfoMockのメンバー変数passwdのアノテーションが読み出せている() {
        try {
            System.out.println("readFieldInfo");
            
            Field f = UserInfoMock.class.getDeclaredField("passwd");
            f.setAccessible(true);
            
            FieldInfo result = AnnotationReader.readFieldInfo(f);
            
            assertEquals("開始位置は8", 8, result.getStartPosition());
            assertEquals("データ長は8", 8, result.getLength());
            assertEquals("アラインメントは左", TextRecordField.Alignment.Left, result.getAlignment());

        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(AnnotationReaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void UserInfoMockのメンバー変数userNameのアノテーションが読み出せている() {
        try {
            System.out.println("readFieldInfo");

            Field f = UserInfoMock.class.getDeclaredField("userName");
            f.setAccessible(true);

            FieldInfo result = AnnotationReader.readFieldInfo(f);

            assertEquals("開始位置は16", 16, result.getStartPosition());
            assertEquals("データ長は32", 32, result.getLength());
            assertEquals("アラインメントは右", TextRecordField.Alignment.Right, result.getAlignment());

        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(AnnotationReaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
