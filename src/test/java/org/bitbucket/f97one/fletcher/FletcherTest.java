/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.f97one.fletcher;

import org.bitbucket.f97one.fletcher.Fletcher;
import java.nio.charset.Charset;
import org.bitbucket.f97one.fletcher.dto.AllSupportedFieldsIncluded;
import org.bitbucket.f97one.fletcher.dto.AllSupportedFieldsIncluded2;
import org.bitbucket.f97one.fletcher.dto.PaddingUserInfo;
import org.bitbucket.f97one.fletcher.dto.UserInfoMS932;
import org.bitbucket.f97one.fletcher.dto.UserInfoMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HAJIME
 */
public class FletcherTest {
    
    public FletcherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void 文字列からオブジェクトを生成できる() {
        String test = "user0001p@ssw0rd               I am Test User 0125 ";
        
        UserInfoMock mock = new Fletcher().fromStr(test, UserInfoMock.class);

        assertNotNull("オブジェクトに変換できている", mock);
        assertTrue(mock instanceof UserInfoMock);
        assertEquals("user0001", mock.getUserId());
        assertEquals("p@ssw0rd", mock.getPasswd());
        assertEquals("I am Test User 01", mock.getUserName());
        assertEquals(25, mock.getAge());
    }
    
    @Test
    public void 文字列からサポートしているすべてフィールドを持つオブジェクトを生成できる() {
        String test = "10  20  1000    2000    2 4 abcdef  0.123   1.234   2.345   3.456   ACtrue  false ";
        
        AllSupportedFieldsIncluded actual = new Fletcher().fromStr(test, AllSupportedFieldsIncluded.class);
        
        assertNotNull("オブジェクトに変換できている", actual);
        assertTrue(actual instanceof AllSupportedFieldsIncluded);
        assertEquals(10, actual.getIntField01());
        assertEquals(20, actual.getIntegerField01().intValue());
        assertEquals((long) 1000, actual.getLongField01());
        assertEquals((long) 2000, actual.getLongField02().longValue());
        assertEquals((short) 2, actual.getShortField01());
        assertEquals((short) 4, actual.getShortField02().shortValue());
        assertEquals("abcdef", actual.getStringField01());
        assertEquals(0.123f, actual.getFloatField01(), 0);
        assertEquals(1.234f, actual.getFloatField02(), 0);
        assertEquals(2.345, actual.getDblField01(), 0);
        assertEquals(3.456, actual.getDblField02(), 0);
        assertEquals('A', actual.getCharField01());
        assertEquals('C', actual.getCharField02().charValue());
        assertTrue(actual.isBoolField01());
        assertFalse(actual.getBoolField02());
    }
    
    @Test
    public void オブジェクトから文字列を生成できる() {
        UserInfoMock uim = new UserInfoMock();
        uim.setUserId("user0001");
        uim.setPasswd("p@ssw0rd");
        uim.setUserName("I am Test User 01");
        uim.setAge(25);
        
        String expected = "user0001p@ssw0rd               I am Test User 0125 ";
        
        String actual = new Fletcher().transform(uim);
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void サポートするすべてのフィールドを文字列から生成できる() {
        
        String expected = "10  20  1000    2000    2 4 abcdef  0.123   1.234   2.345   3.456   ACtrue  false ";
        
        AllSupportedFieldsIncluded asfi = new AllSupportedFieldsIncluded();
        asfi.setIntField01(10);
        asfi.setIntegerField01(20);
        asfi.setLongField01(1000);
        asfi.setLongField02((long) 2000);
        asfi.setShortField01((short) 2);
        asfi.setShortField02((short) 4);
        asfi.setStringField01("abcdef");
        asfi.setFloatField01(0.123f);
        asfi.setFloatField02(1.234f);
        asfi.setDblField01(2.345);
        asfi.setDblField02(3.456);
        asfi.setCharField01('A');
        asfi.setCharField02('C');
        asfi.setBoolField01(true);
        asfi.setBoolField02(false);
        
        String actual = new Fletcher().transform(asfi);
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void byte配列からオブジェクトを生成できる() {
        String base = "10  20  1000    2000    2 4 abcdef  0.123   1.234   2.345   3.456   ACtrue  false ";
        byte[] test = base.getBytes(Charset.forName(Fletcher.Encodings.MS932));
        
        AllSupportedFieldsIncluded2 actual = new Fletcher().fromBytes(test, AllSupportedFieldsIncluded2.class);
        
        assertNotNull("オブジェクトに変換できている", actual);
        assertTrue(actual instanceof AllSupportedFieldsIncluded2);
        assertEquals(10, actual.getIntField01());
        assertEquals(20, actual.getIntegerField01().intValue());
        assertEquals((long) 1000, actual.getLongField01());
        assertEquals((long) 2000, actual.getLongField02().longValue());
        assertEquals((short) 2, actual.getShortField01());
        assertEquals((short) 4, actual.getShortField02().shortValue());
        assertEquals("abcdef", actual.getStringField01());
        assertEquals(0.123f, actual.getFloatField01(), 0);
        assertEquals(1.234f, actual.getFloatField02(), 0);
        assertEquals(2.345, actual.getDblField01(), 0);
        assertEquals(3.456, actual.getDblField02(), 0);
        assertEquals('A', actual.getCharField01());
        assertEquals('C', actual.getCharField02().charValue());
        assertTrue(actual.isBoolField01());
        assertFalse(actual.getBoolField02());

    }
    
    @Test
    public void サポートするすべてのフィールドをbyte配列から生成できる() {
        String base = "10  20  1000    2000    2 4 abcdef  0.123   1.234   2.345   3.456   ACtrue  false ";
        byte[] expected = base.getBytes(Charset.forName(Fletcher.Encodings.MS932));

        AllSupportedFieldsIncluded2 asfi = new AllSupportedFieldsIncluded2();
        asfi.setIntField01(10);
        asfi.setIntegerField01(20);
        asfi.setLongField01(1000);
        asfi.setLongField02((long) 2000);
        asfi.setShortField01((short) 2);
        asfi.setShortField02((short) 4);
        asfi.setStringField01("abcdef");
        asfi.setFloatField01(0.123f);
        asfi.setFloatField02(1.234f);
        asfi.setDblField01(2.345);
        asfi.setDblField02(3.456);
        asfi.setCharField01('A');
        asfi.setCharField02('C');
        asfi.setBoolField01(true);
        asfi.setBoolField02(false);

        byte[] actual = new Fletcher().transformToBin(asfi);

        Assert.assertArrayEquals(expected, actual);

    }
    
    @Test
    public void 全角文字を含むMS932の文字列からDTOを生成できる() {
        String ms932Text = "user0001p@ssw0rd              私はuser0001です。25 ";
        
        UserInfoMS932 ms932Obj = new Fletcher().fromStr(ms932Text, UserInfoMS932.class);
        
        assertNotNull("オブジェクトに変換できている", ms932Obj);
        assertTrue(ms932Obj instanceof UserInfoMS932);
        assertEquals("user0001", ms932Obj.getUserId());
        assertEquals("p@ssw0rd", ms932Obj.getPasswd());
        assertEquals("私はuser0001です。", ms932Obj.getUserName());
        assertEquals(25, ms932Obj.getAge());

    }
    
    @Test
    public void 全角文字をフィールドの値に含むオブジェクトからCP932の文字列を生成できる() {
        UserInfoMS932 ms932Obj = new UserInfoMS932();
        
        ms932Obj.setUserId("user0001");
        ms932Obj.setPasswd("p@ssw0rd");
        ms932Obj.setUserName("私はuser0001です。");
        ms932Obj.setAge(25);
        
        String expected = "user0001p@ssw0rd              私はuser0001です。25 ";
        String actual = new Fletcher().transform(ms932Obj);
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void ゼロパディングされた文字列をオブジェクト化できる() {
        String test = "00000001p@ssw0rd               I am Test User 01009";
        
        PaddingUserInfo mock = new Fletcher().fromStr(test, PaddingUserInfo.class);

        assertNotNull("オブジェクトに変換できている", mock);
        assertTrue(mock instanceof PaddingUserInfo);
        assertEquals(1, mock.getUserId());
        assertEquals("p@ssw0rd", mock.getPasswd());
        assertEquals("I am Test User 01", mock.getUserName());
        assertEquals(9, mock.getAge());
    }
    
    @Test
    public void オブジェクトからゼロパディングされた文字列を生成できる() {
        PaddingUserInfo info = new PaddingUserInfo();
        
        info.setUserId(1);
        info.setPasswd("p@ssw0rd");
        info.setUserName("I am Test User 01");
        info.setAge(9);
        
        String expected = "00000001p@ssw0rd               I am Test User 01009";
        String actual = new Fletcher().transform(info);
        
        assertEquals(expected, actual);
    }
}
